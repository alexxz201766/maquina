//
//  maquina1.hpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 08/01/21.
//

#ifndef maquina1_hpp
#define maquina1_hpp
#include "maquina.hpp"
#include <stdio.h>

 class maquina1: public maquina {
protected:
    float voltsm,ampersm;    //valores de energia
    float watsm;               // wats
    float tiempom;
    float temperaturam;
public:
    maquina1();
    maquina1(long,long);
    maquina1(long,long,long,long);
    maquina1(long,long,long,long,long);
    float produccionG(void);
    long temperaturaG(void);
    float acabadoG(void);
    void settiempoG(long,long,long,long,long);
    float getvolts();
    float getampers();
    float getwats();
    float gettiempo();
    float gettemperatura();

   virtual long costoG(long);
     virtual void mensaje();
};

#endif /* maquina1_hpp */
