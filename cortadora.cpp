//
//  cortadora.cpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 14/01/21.
//
#include <math.h>
#include "cortadora.hpp"
#include <iostream>

cortadora::cortadora():maquina1(){}

cortadora::cortadora(long volts,long ampers,long wats,long temperatura,long tiempo):maquina1(volts,ampers,wats,temperatura,tiempo){}

void cortadora::mensaje(){
   std::cout<<"\t• Esta maquina puede dar el mayor rendimiento y lleva nuestras garantias. :)";
}
