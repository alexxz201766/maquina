//
//  main.cpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 08/01/21.

#include "maquina.hpp"
#include "maquina1.hpp"
#include "cortadora.hpp"
#include "esmeril.hpp"
#include <iostream>
using namespace std;
int main() {
    cout << "\n\n\n\n\t\t\t\tbienvenido a creadoras ""ARUBA"" en donde le podemos crear la maquina que usted desee. \n\n\n\n";
    
    float volts,ampers,wats,temperatura,tiempo;
    
    cout<<"\t- Dame el valor de los volts: ";
    cin>>volts;
    cout<<"\t- Dame el valor del amperaje: ";
    cin>>ampers;
    cout<<"\t- Dame el valor de los wats: ";
    cin>>wats;
    cout<<"\t- Dame la temperatura en grados centigrados: ";
    cin>>temperatura;
    cout<<"\t- Dame el tiempo de procesamiento en minutos: ";
    cin>>tiempo;
    
    maquina1 t(volts,ampers,wats,temperatura,tiempo);
    maquina1 u;
    
    cout << "\n\n\t• Con los valores dados la produccion de esa maquina sera de: " << t.produccionG()<<" cortes totales.\n\n";
    cout << "\t• La temperatura de esta maquina sera de: " << t.temperaturaG()<<"º grados centigrados.\n\n";
    cout << "\t• La maquina termirara los cortes en un periodo de: " << t.acabadoG()<<" minutos.\n\n";
    cout << "\t• Debido al tiempo en el que la maquina termina el trabajo su costo sera de $ " << t.costoG(5)<< " pejedolares.\n\n";
    u.mensaje();
    
    cortadora e(3,4,5,6,3);
    cortadora e1;
    
    cout << "\n\n\n\n\t\t\t\t\t\tPor otra parte podemos ofrecerte estas maquinas ya diseñadas.";
    cout <<"\n\n\n\t\t\t\t---> Cortadora modelo (FIRE2020) <---";
    cout <<"\n\n\tEsta maquina trabaja con 3 volts, 6 ampers, su temperatura llegara a 6 grados centigrados y el tiempo de dependera de como se use.";
    cout << "\n\n\tLa produccion de esa maquina sera de: " << e.produccionG()<<" cortes totales.\n\n";
    cout << "\tLa temperatura de esta maquina llegara a: " << e.temperaturaG()<<"º grados centigrados.\n\n";
    cout << "\tLa maquina terminara los cortes en un periodo de: " << e.acabadoG()<<" minutos.\n\n";
    cout << "\tDebido al tiempo en el que la maquina termina el trabajo su costo sera de $ " << e.costoG(5)<< " pejedolares.\n\n";
     e1.mensaje();
    cout << "\n\n";
    
    esmeril trr(8.4,12.3,97,198,280);
    
    cout << "\n\n\t\t\t\t---> BLEKBAY TORNO 7000 <---";
    cout <<"\n\n\tEsta maquina trabaja con 8.4 volts, 12.3 ampers, su temperatura llegara a 198 grados centigrados y el tiempo de dependera de como se use, pero puede tardar 5 horas.";
    cout << "\n\n\tLa produccion de esa maquina sera de: " << trr.produccionG()<<" cortes totales.\n\n";
    cout << "\tLa temperatura de esta maquina llegara a: " << trr.temperaturaG()<<"º grados centigrados.\n\n";
    cout << "\tLa maquina terminara los cortes en un periodo de: " << trr.acabadoG()<<" minutos.\n\n";
    cout << "\tDebido al tiempo en el que la maquina termina el trabajo su costo sera de $ " << trr.costoG(5)<< " pejedolares.\n\n";
    trr.mensaje();
   
    return 0;
}
