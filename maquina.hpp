//
//  maquina.hpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 08/01/21.
//

#ifndef maquina_hpp
#define maquina_hpp

#include <stdio.h>

class maquina{
private:
    float tiempoG;

public:
    maquina();
    maquina(long);
    float produccionG(void);
    double temperaturaG(void);
    void settiempoG(long);
    float gettiempoG(void);
    float costoG(long);
};


#endif /* maquina_hpp */
