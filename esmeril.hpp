//
//  esmeril.hpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 15/01/21.
//

#ifndef esmeril_hpp
#define esmeril_hpp
#include "maquina1.hpp"

#include <stdio.h>

class esmeril: public maquina1 {
public:
    esmeril();
    esmeril(long,long,long,long,long);
    float produccion(void);
    void mensaje();
    
};

#endif /* esmeril_hpp */
