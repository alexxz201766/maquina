//
//  maquina1.cpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 08/01/21.
//
#include <math.h>
#include <iostream>

#include "maquina1.hpp"
using namespace std;

maquina1::maquina1():maquina(5){
    voltsm=0;
    ampersm=0;
    watsm=0;
    tiempom=0;
    temperaturam=0;
};
maquina1::maquina1(long volts,long ampers):maquina(2){
    voltsm=volts;
    ampersm=ampers;
    watsm=volts*ampers;
}
maquina1::maquina1(long volts,long ampers,long wats,long temperatura):maquina(3){
    voltsm=volts;
    ampersm=ampers;
    watsm=wats;
    temperaturam=temperatura;
    tiempom=wats*temperatura;
}
maquina1::maquina1(long volts,long ampers,long wats,long temperatura,long tiempo):maquina(4){
    voltsm=volts;
    ampersm=ampers;
    watsm=wats;
    temperaturam=temperatura;
    tiempom=tiempo;
}
float maquina1::produccionG(){
    return ((ampersm*tiempom)/temperaturam);
}
long maquina1::temperaturaG(){
    return ((pow(watsm, 1))*(tiempom))/temperaturam;
}
float maquina1::acabadoG(){
    
    return tiempom*(pow(watsm, 2))/temperaturam;
}
void maquina1::settiempoG(long volts,long ampers,long wats,long temperatura,long tiempo){
    voltsm=volts;
    ampersm=ampers;
    watsm=wats;
    temperaturam=temperatura;
    tiempom=tiempo;
}
float maquina1::getvolts(){return voltsm;}
float maquina1::getampers(){return ampersm;}
float maquina1::getwats(){return watsm;}
float maquina1::gettiempo(){return temperaturam;}
float maquina1::gettemperatura(){return tiempom;}

long maquina1::costoG(long costo){
    
    costo=((pow(tiempom,2)*(ampersm*pow(tiempom,2))/(temperaturam)));
    return costo;
}
 void maquina1::mensaje(){
     cout<<"\t• Esta maquina puede dar el mayor rendimiento, sin embargo no deje pasar su mantenimiento :)"<<endl;
}
