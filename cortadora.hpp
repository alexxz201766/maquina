//
//  cortadora.hpp
//  proyecmaquinaa
//
//  Created by ALEJANDRO TOLENTINO on 14/01/21.
//

#ifndef cortadora_hpp
#define cortadora_hpp
#include "maquina1.hpp"
#include "maquina.hpp"
#include <stdio.h>

class cortadora: public maquina1 {
    public:
    cortadora();
    cortadora(long,long,long,long,long);
    void mensaje();
};

#endif /* cortadora_hpp */
